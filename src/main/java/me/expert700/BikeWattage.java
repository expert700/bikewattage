package me.expert700;

import java.util.Scanner;

public class BikeWattage {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("What speed do you wish to go? (mph): ");
        double speed = in.nextDouble();
        double speedMetric = speed * 0.44704;
        double wattage = 0.276 * (Math.pow(speed, 3)) + 7.203 * speed;
        System.out.printf("\nThe wattage required to go %f mph is %f.", speed, wattage);
    }
}
